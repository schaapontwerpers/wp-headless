# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.8.0] - 2025-02-18

### Added

- Options page for frontend and revalidate url.

### Changed

- Changed classes folder to inc instead of src for consistency across plugins.

### Fixed

- Changed revalidation hook to wp_after_insert_post so that meta data and terms are also updated before revalidating.

[1.8.0]: https://bitbucket.org/schaapontwerpers/wp-headless/branches/compare/1.8.0%0D1.7.1
