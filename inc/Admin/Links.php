<?php

namespace WordPressHeadless\Admin;

use WordPressPluginAPI\FilterHook;

class Links implements FilterHook
{
    /**
     * Subscribe functions to corresponding actions
     */
    public static function getFilters(): array
    {
        return [
            "preview_post_link" => ["setHeadlessPreviewLink", 10, 2],
            'wp_insert_post_data' => ['overridePostLinks', 10, 1],
        ];
    }

    /**
     * Customize the preview button in the WordPress admin to point to the headless client.
     */
    public function setHeadlessPreviewLink(string $link, \WP_Post $post)
    {
        $frontendUrl = function_exists('get_field') ?
            get_field('frontend_url', 'options') :
            null;

        if (!$frontendUrl) {
            return $link;
        }

        $queryArgs = [
            'id' => $post->ID,
            'token' => defined('PREVIEW_SECRET_TOKEN')
                ? PREVIEW_SECRET_TOKEN
                : '',
        ];

        if ($post->post_type !== 'page') {
            $queryArgs['route'] = $this->getPermalink($post->post_type);
        }

        // Preview link will have format:
        // <domain>/api/preview?id=<post-id>&token=<preview-token>&route=<route>
        return add_query_arg($queryArgs, "{$frontendUrl}/api/preview");
    }

    /**
     * Get the correct permalink structure
     */
    private function getPermalink(string $postType): string
    {
        global $wp_rewrite;

        $postTypeObject = get_post_type_object($postType);
        $slug = $postTypeObject->name;
        $basePermalink = $postTypeObject->rewrite
            ? $postTypeObject->rewrite['slug']
            : $slug;
        $filteredPermalink = '';

        switch ($slug) {
            case 'post':
                $filteredPermalink = str_replace(
                    '/',
                    '',
                    str_replace(
                        '%postname%',
                        '',
                        $wp_rewrite->permalink_structure
                    )
                );

                break;

            default:
                $filteredPermalink = $basePermalink;

                break;
        }

        return $filteredPermalink;
    }

    public function overridePostLinks($data)
    {
        $frontendUrl = function_exists('get_field') ?
            get_field('frontend_url', 'options') :
            null;
        $postContent = $data['post_content'];

        if ($frontendUrl) {
            // Replace all references to WP HOME by Next.js link
            $newPostContent = str_ireplace(
                home_url(),
                $frontendUrl,
                $postContent
            );

            // Reset all references to images to a WP HOME link
            $data['post_content'] = str_ireplace(
                $frontendUrl . '/app/uploads',
                home_url() . '/app/uploads',
                $newPostContent
            );
        }

        return $data;
    }
}
