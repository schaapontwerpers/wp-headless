<?php

namespace WordPressHeadless;

use WordPressPluginAPI\Manager;

class Init
{
    private $hooks;

    public function __construct()
    {
        $this->hooks = [
            new Admin\Links(),
            new Admin\OptionsPages(),
            new NextJS\Redirect(),
            new NextJS\Revalidate(),
            new Plugins\Yoast(),
            new RestAPI\Links(),
        ];

        // Run PluginAPI registration
        $manager = new Manager();
        foreach ($this->hooks as $class) {
            $manager->register($class);
        }
    }
}
