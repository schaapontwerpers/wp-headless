<?php

namespace WordPressHeadless\NextJS;

use WordPressPluginAPI\ActionHook;
use WordPressPluginAPI\FilterHook;

class Redirect implements ActionHook, FilterHook
{
    /**
     * Subscribe functions to corresponding actions
     */
    public static function getActions(): array
    {
        return [
            'template_redirect' => 'redirect',
        ];
    }

    /**
     * Subscribe functions to corresponding filters
     */
    public static function getFilters(): array
    {
        return [
            'redirect_canonical' => ['canonicalRedirect', 10, 2],
        ];
    }

    /**
     * Disable wp-activate redirect for signups
     */
    public function canonicalRedirect($redirectUrl, $requestedUrl)
    {
        return str_contains($requestedUrl, '/wp/wp-activate.php') ?
            $requestedUrl :
            $redirectUrl;
    }

    /**
     * Revalidate Next.js when a post gets saved
     */
    public function redirect()
    {
        $frontendUrl = function_exists('get_field') ?
            get_field('frontend_url', 'options') :
            null;

        if ($frontendUrl) {
            $currentPath = $_SERVER['REQUEST_URI'];

            if (
                !str_starts_with($currentPath, '/wp/wp-') &&
                !str_starts_with($currentPath, '/wp-')
            ) {
                wp_redirect($frontendUrl . $currentPath);

                exit;
            }
        }
    }
}
