<?php

namespace WordPressHeadless\NextJS;

use WordPressPluginAPI\ActionHook;

class Revalidate implements ActionHook
{
    /**
     * Subscribe functions to corresponding actions
     */
    public static function getActions(): array
    {
        return [
            'wp_after_insert_post' => ['onDemandRevalidation', 10, 4],
            'wp_update_nav_menu' => 'onDemandMenuRevalidation',
        ];
    }

    /**
     * Revalidate Next.js when a post gets saved
     */
    public function onDemandRevalidation($postId, $post, $update, $postBefore)
    {
        // These constants are required. If they're not here, bail...
        $revalidateUrl = function_exists('get_field') ?
            get_field('revalidate_url', 'options') :
            null;

        if (!defined('PREVIEW_SECRET_TOKEN') || !$revalidateUrl) {
            return;
        }

        // No post ID? Bail...
        if (!$postId) {
            return;
        }

        $ignorePostTypes = [
            'nav_menu_item',
            'oembed_cache',
            'user_request',
            'wp_template',
            'wp_template_part',
            'wp_global_styles',
            'wp_navigation',
        ];

        if (!in_array($post->post_type, $ignorePostTypes)) {
            // Send POST request to the frontend and revalidate the cache.
            wp_remote_post($revalidateUrl . '/api/revalidate/paths', [
                'headers' => [
                    'Content-Type' => 'application/json; charset=utf-8',
                ],
                'body' => json_encode([
                    'secret' => PREVIEW_SECRET_TOKEN,
                ]),
            ]);
        }
    }

    /**
     * Revalidate Next.js when a menu gets saved
     */
    public function onDemandMenuRevalidation()
    {
        // These constants are required. If they're not here, bail...
        $revalidateUrl = function_exists('get_field') ?
            get_field('revalidate_url', 'options') :
            null;

        if (!defined('PREVIEW_SECRET_TOKEN') || !$revalidateUrl) {
            return;
        }

        // Send POST request to the frontend and revalidate the cache.
        wp_remote_post($revalidateUrl . '/api/revalidate/tag', [
            'headers' => [
                'Content-Type' => 'application/json; charset=utf-8',
            ],
            'body' => json_encode([
                'secret' => PREVIEW_SECRET_TOKEN,
                'tag' => 'menu',
            ]),
        ]);
    }
}
