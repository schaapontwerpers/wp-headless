<?php

namespace WordPressHeadless\Plugins;

use WordPressPluginAPI\FilterHook;
use Yoast\WP\SEO\Context\Meta_Tags_Context;

class Yoast implements FilterHook
{
    /**
     * Subscribe functions to corresponding filters
     */
    public static function getFilters(): array
    {
        return [
            'wpseo_sitemap_url' => 'changeUrl',
            'wpseo_canonical' => 'changeUrl',
            'wpseo_opengraph_url' => 'changeUrl',
            'wpseo_schema_graph' => ['changeSchemaUrl', 10, 2]
        ];
    }

    /**
     * Update references to home URL
     */
    public function changeUrl(string $originalUrl): string
    {
        $homeUrl = get_home_url();
        $frontendUrl = function_exists('get_field') ?
            get_field('frontend_url', 'options') :
            null;

        return $frontendUrl ?
            str_replace($homeUrl, $frontendUrl, $originalUrl) :
            $originalUrl;
    }

    /**
     * Update references to home URL in schema
     */
    public function changeSchemaUrl(
        array $schema,
        Meta_Tags_Context $context
    ): array {
        $homeUrl = get_home_url();
        $frontendUrl = function_exists('get_field') ?
            get_field('frontend_url', 'options') :
            null;

        if ($frontendUrl) {
            $this->traverseSchema($schema, $homeUrl, $frontendUrl);
        }

        return $schema;
    }

    private function traverseSchema(
        array &$schema,
        string $homeUrl,
        string $frontendUrl
    ): void {
        foreach ($schema as $key => $value) {
            if (is_array($value)) {
                $this->traverseSchema($schema[$key], $homeUrl, $frontendUrl);
            } else if (is_string($value)) {
                if (
                    strpos($value, $homeUrl) !== false &&
                    strpos($value, '/app/uploads/') === false
                ) {
                    $schema[$key] = str_replace(
                        $homeUrl,
                        $frontendUrl,
                        $value
                    );
                }
            }
        }
    }
}
