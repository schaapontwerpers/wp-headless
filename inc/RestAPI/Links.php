<?php

namespace WordPressHeadless\RestAPI;

use WordPressPluginAPI\FilterHook;

class Links implements FilterHook
{
    /**
     * Subscribe functions to corresponding filters
     */
    public static function getFilters(): array
    {
        $filters = [];

        $postTypes = get_post_types_by_support(['editor']);

        foreach ($postTypes as $postType) {
            $filters['rest_prepare_' . $postType] = [
                'setRestPreviewLink',
                10,
                2,
            ];
        }

        return $filters;
    }

    /**
     * Customize the REST preview link to point to the headless client.
     */
    public function setRestPreviewLink(
        \WP_REST_Response $response,
        \WP_Post $post
    ) {
        if ($post->post_status === 'draft') {
            $response->data['link'] = get_preview_post_link($post);
        } elseif ($post->post_status === 'publish') {
            $frontendUrl = function_exists('get_field') ?
                get_field('frontend_url', 'options') :
                null;

            if ($frontendUrl) {
                return $response;
            }

            $permalink = get_permalink($post);
            $wpHome = home_url();

            // Replace site URL if present.
            if (stristr($permalink, $wpHome) !== false) {
                $permalink = str_ireplace($wpHome, $frontendUrl, $permalink);
            }

            // Return URL based on post name.
            $response->data['link'] = $permalink;

            if (array_key_exists('permalink_template', $response->data)) {
                $permalinkTemplate = $response->data['permalink_template'];

                // Replace site URL if present.
                if (stristr($permalinkTemplate, $wpHome) !== false) {
                    $permalinkTemplate = str_ireplace(
                        $wpHome,
                        $frontendUrl,
                        $permalinkTemplate
                    );
                }

                $response->data['permalink_template'] = $permalinkTemplate;
            }
        }

        return $response;
    }
}
