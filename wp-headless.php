<?php
/*
Plugin Name:      WP Headless
Plugin URI:       https://packagist.org/packages/schaapdigitalcreatives/wp-headless
Description:      Required WordPress plugin when using it as a headless CMS
Version:          1.8.0
Author:           Schaap Digital Creatives
Author URI:       https://schaapontwerpers.nl/
License:          GNU General Public License
Requires Plugins: advanced-custom-fields-pro
*/

new WordPressHeadless\Init();
